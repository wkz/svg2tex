import os
import shutil
import argparse


def cutter(svgfile):
    datapath, filename = os.path.split(__file__)
    newpath = datapath + '\\' + svgfile
    print(newpath)
    with open(newpath, 'r') as tmp:
        lines = tmp.readlines()

    line_list = []

    for line in lines:
        if "\\put(0,0)" in line:
            line_list.append([lines.index(line)])

    start = line_list[0]
    end = line_list[len(line_list) - 1]

    short_list = []
    for number, line in enumerate(lines):
        if number not in range(start[0]+1, end[0]+1):
            short_list.append(line)

    return short_list


def savenewdata(svgfile, lines):

    # svgfile = "input.pdf_tex"

    datapath, filename = os.path.split(__file__)
    newpath = datapath + '\\' + svgfile
    source = newpath

    # Sicherheitskopie
    destination = newpath + "_bak"
    shutil.copyfile(source, destination)

    # Geänderte Daten schreiben

    with open(newpath, 'w') as fh:
        for line in lines:
            fh.write(line)


if __name__ == '__main__':

    help_desc = """

To include graphics from exported Inkscape SVG's you can do
the following steps: \n
\n
inkscape -D -z -C -f figure.svg figure.pdf --export-latex

inkscape -D -z -f figure.svg -A figure.pdf


Problem: The generated figure.pdf_tex has inststuctions of an
multipage PDF. This tool is cutting all put statements except
for the first page.

A description in German ist available at:
\n\n
https://inkubator.sudile.com/kurse/latex/html/faq/qi-images/svg-einbinden.html

"""

    help_svg = "SVG file as parameter"

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        prog="svg2pdf_tex.py",
        description=help_desc)
    parser.add_argument('svg',
                        type=argparse.FileType('r'),
                        help=help_svg)
    args = parser.parse_args()
    lines = cutter(args.svg.name)
    savenewdata(args.svg.name, lines)
