class Cutter:
    """Cutter

    Class to shorten a pdf_tex file.

    >>> import wkz.svg2tex.cutter as cut
    >>> pdf_tex = cut.Cutter()
    >>> pdf_tex.shorten()
    ['Nothing to cut, no line starts with put in this text']

    """

    def __init__(self, pdf_tex=""):
        self.line_list = []
        self.short_list = []
        self.pdf_tex = pdf_tex

    def shorten(self):
        """cut all lines with put, except the first one

        :param pdf_tex: Latex structure as text generated
                        as export from Inkscape:

        inkscape -D -z -C -f figure.svg figure.pdf --export-latex
        """

        if not self.pdf_tex:
            return ["Nothing to cut, no line starts with put in this text"]

        for line in self.pdf_tex:
            if "\\put(0,0)" in line:
                self.line_list.append([self.pdf_tex.index(line)])

        start = self.line_list[0]
        end = self.line_list[len(self.line_list) - 1]

        for number, line in enumerate(self.pdf_tex):
            if number not in range(start[0]+1, end[0]+1):
                self.short_list.append(line)

        return self.short_list


if __name__ == "__main__":
    import doctest
    doctest.testmod()
