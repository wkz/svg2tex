# -*- coding: utf-8 -*-

""" Module

wkz  -- Namespace package for a collection of tools

"""

# from wkz.svg2tex import cutter

name = "wkz.svg2tex"

__author__ = """Peter Koppatz"""
__email__ = 'repos@koppatz.comg'
__version__ = '0.1.0'
