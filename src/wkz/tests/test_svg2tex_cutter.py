
"""
Tests for cutter.py

"""

from wkz.svg2tex.cutter import Cutter
import os


def test_Cutter():
    """Tests  shorten operation """

    exec = Cutter()
    result = exec.shorten()

    assert result == ["Nothing to cut, no line starts with put in this text"]


def test_pdf_tex_text():
    """Tests  shorten operation """

    TESTFILENAME = '/data/input.pdf_tex'
    datapath, filename = os.path.split(__file__)
    with open(datapath+TESTFILENAME, 'r') as fh:
        text_input = fh.readlines()

    TESTFILENAME = '/data/result.pdf_tex'
    datapath, filename = os.path.split(__file__)
    with open(datapath+TESTFILENAME, 'r') as fh:
        text_result = fh.read()

    exec = Cutter(text_input)
    result = exec.shorten()

    assert "".join(result).count('put(') == text_result.count("put(")
