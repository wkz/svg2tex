#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requires = [
    'setuptools',
]


setup_requirements = ['pytest-runner', ]
test_requirements = ['pytest', ]

setup(
    author="Peter Koppatz",
    author_email='repos@koppatz.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: End Users/Desktop',
        'Topic ::Printing :: Documentation::Text Processing',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.10',
    ],
    description="wkz.svg2tex:",
    install_requires=requires,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    long_description_content_type='text/x-rst',
    include_package_data=True,
    keywords='svg2tex',
    namespace_packages=['wkz'],
    packages=['wkz.svg2tex'],
    name='wkz.svg2tex',
    package_dir={'': 'src'},
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.io',
    download_url='https://gitlab.com/wkz-tools/svg2tex/src/default/',
    version='0.1.0',
    zip_safe=False,
    scripts=['bin/svg2pdf_and_tex.py']
)
