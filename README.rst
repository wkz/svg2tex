===================
Module: wkz.svg2tex
===================

* Free software: MIT license
* Documentation: https://pyhasse.org


Features
--------

Convert SVG-File with Inkscape and correct content
