wkz.svg2tex
===========

Getting Started
---------------

- Change directory into your newly created project.

  .. code::
  
     cd /go/to/your/project/folder

- Create a Python virtual environment.

  .. code::

     python3 -m venv env-svg

- Upgrade packaging tools.

  .. code::

    env/bin/pip install --upgrade pip setuptools
    env/bin/pip install --upgrade pip pip

- Install wkz.svg2tex

  .. code::
    
     pip install wkz.svg2tex
    
- Read the documentation

  You can find the documentation at:
  
  ???
